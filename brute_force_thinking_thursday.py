#!/usr/bin/env python3
import sys
import argparse
import logging
from tqdm import tqdm
import random


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(
        description="Brute force Shellene's Thinking Thursday challenge"
    )
    parser.add_argument(
        "-v", "--verbose", help="Be verbose", action="store_true", dest="verbose"
    )

    parser.add_argument(
        "-m", "--max-guesses", type=int, help="Maximum guesses", default=100000
    )
    parser.add_argument("target_number", help="Target Number", type=int)
    parser.add_argument(
        "possible_numbers", nargs="+", help="Possible numbers", type=int
    )

    return parser.parse_args()


# Return both the result, and a human readble version of the math operation


def norm_sum(a, b):
    return [a + b, f"{a} + {b}"]


def norm_minus(a, b):
    if random.getrandbits(1) == 1:
        return [a - b, f"{a} - {b}"]
    else:
        return [b - a, f"{b} - {a}"]


def norm_power(a, b):
    # If you have to power up way high, it will melt my laptop
    max_power = 100000
    if (random.getrandbits(1) == 1) or (a > max_power):
        return [a ** b, f"{a} ^ {b}"]
    else:
        return [b ** a, f"{b} ^ {a}"]


def norm_multiply(a, b):
    return [a * b, f"{a} x {b}"]


def norm_divide(a, b):
    if random.getrandbits(1) == 1:
        return [a / b, f"{a} / {b}"]
    else:
        return [b / a, f"{b} / {a}"]


def main():
    args = parse_args()
    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
    else:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.INFO)

    # List of math operators
    operators = [norm_minus, norm_power, norm_multiply, norm_sum, norm_divide]

    current = None
    attempts = 0
    numbers = args.possible_numbers
    final_operation = None
    good_equations = []

    #   while attempts < args.max_guesses:
    for attempt in tqdm(range(args.max_guesses)):
        random.shuffle(numbers)
        previous = None
        math_operations = []
        for item in numbers:
            # Skip first item
            if not previous:
                previous = item
                continue

            # Get a random math operator
            random.shuffle(operators)
            op = operators[0]

            # Parse the results
            current, math_operation = op(previous, item)
            math_operations.append(math_operation)

            previous = current
        # attempts += 1

        # Did we guess it right??
        if current == args.target_number:
            final_operation = math_operations
            if final_operation not in good_equations:
                print(
                    f"Found unique equation after {attempts} random attempts: {final_operation}"
                )
                good_equations.append(final_operation)

    return 0


if __name__ == "__main__":
    sys.exit(main())
